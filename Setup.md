# Setting up a development environment

## IDEs
* Install Intellij, community edition is ok (for the server part)
  * In Intellij, go to Settings &rarr; Build, Execution, Deployment &rarr; Compiler &rarr; Kotlin Compiler
  * set Target JVM version to 1.8
* Install Android Studio (for the client part)
* Install Docker
* Install Git
* Install OpenJDK 8
* Install Maven

## Repositories
* Clone some repositories from https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch, i.e.
  ```
  git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/openhitch.git \
  && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/graphhopper.git \
  && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/openhitch-protocol.git \
  && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/graphhopper-openhitch.git \
  && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/geocoder-converter.git \
  && git clone git@gitlab.com:developersforfuture/wevsclimatecrisis/openhitch/openhitch-android.git
  ```

## Please Help
Open openhitch/Setup.md in a text editor and note all difficulties you have and how you dealt with them. Upload when you are done

## Initial build
To make sure that everything is in a working state, build all projects in the required order. This will take a while. If you'd rather do this in your IDE, skip this section an proceed with the next one

```
cd openhitch-protocol
mvn install -T 1C -DskipTests
cd ../graphhopper
mvn install -T 1C -DskipTests
cd ../graphhopper-openhitch
mvn install -T 1C -DskipTests
```

## Prepare and start the Graphhopper backend
You will need a Graphhopper map file to be used by the backend. Download https://download.geofabrik.de/europe/germany/berlin-200101.osm.pbf rename it to europe_germany_berlin.pbf and copy it to graphhopper-openhitch. The graphhopper-openhitch directory should look something like this:
```
$ ls graphhopper-openhitch
LICENSE.txt  config.yml  europe_germany_berlin.pbf  gh-data  logs  pom.xml  src  target
```

Now you are ready to run the Graphhopper backend ‒ congratulations! If you'd rather start this form your IDE, see below.
```
cd graphhopper-openhitch
mvn exec:java
```

You should see some log output:
```
2020-10-13 20:32:44.593 [de.wifaz.oh.server.OpenHitchApplication.main()] INFO  o.e.jetty.server.AbstractConnector - Started application@22551405{HTTP/1.1, (http/1.1)}{localhost:8989}
2020-10-13 20:32:44.600 [de.wifaz.oh.server.OpenHitchApplication.main()] INFO  o.e.jetty.server.AbstractConnector - Started admin@2a3af34f{HTTP/1.1, (http/1.1)}{localhost:8990}
2020-10-13 20:32:44.600 [de.wifaz.oh.server.OpenHitchApplication.main()] INFO  org.eclipse.jetty.server.Server - Started @4130ms
```

Now you can open http://localhost:8989 in your browser and should see the Graphhopper app.  

## Setup IntelliJ

### Protocol Definition
* Open Intelij
* Open Project openhitch-protocol
* specify java 1.8 as project SDK (right click project &rarr; Open Module Settings &rarr; Modules &rarr; Dependencies)
* Open Maven Tool Window by clicking the Maven-Button at the right edge of the IDE and doubleclick "OpenHitch Protocol &rarr; Lifecycle &rarr; install"
  (alternatively start `mvn install` in a terminal)
  This will install the protocol definition in the local Maven central.

### GraphHopper/OpenHitch Server 
* With Intellij open project graphhopper
* Add graphhopper-navigation and graphhopper-openhitch as modules
  (click "File &rarr; New &rarr; Module from Existing Sources", select module directory, select "Import module from exernal model", select "Maven".
  Do update Kotlin compiler to 1.4.10, when asked).
* Click "Build &rarr; Build Project"
* Click "Run &rarr; Edit Configurations"
* Click "+" to add new configuration
  * A menu pops up, choose Application
  * Fill the following values into the form
    * Name: openhitch-server
    * Main class: de.wifaz.oh.server.OpenHitchApplication
    * Program arguments: server config.yml
    * Working directory: /xyz/graphhopper-openhitch (replace xyz by the path where your graphhopper-openhitch directory is)
    * Use classpath of module: graphhopper-openhitch
    * JRE: 1.8
    * Keep defaults for all fields not mentioned here
* Click "Run &rarr; openhitch-server"

## OpenMapTiles (Map server)
Install and start the OpenMapTiles docker container:

`sudo docker run --rm -it -v $(pwd):/data -p 8080:80 klokantech/openmaptiles-server`

If you are using Docker Toolbox with Windows, you will need pass an absolute path to a directory in your home directory and the paths will have to start with two slashes, e.g.

`docker run --rm -it -v //c/Users/<your username>/openhitch/data://data -p 8080:80 klokantech/openmaptiles-server`

Complete the OpenMapTiles installation wizard
* point browser to http://localhost:8080  OpenMapTiles installation wizard should show up
* Go through wizard, choose
  * region: Germany/Berlin
  * style: OSM Bright
  * settings: Serve vector map tiles
  * Follow instructions on how to get a key and download the data

## Build and start Graphhopper Geocoder Converter (relays geo search to Nominatim)
```
cd geocoder-converter
mvn package -DskipTests
java -jar target/graphhopper-geocoder-converter-0.2-SNAPSHOT.jar server converter.yml
```

## Install Mosquitto (MQTT broker)
* On debian, this can be done with

  `sudo apt install mosquitto`

## App
* Open Android Studio
* "Click File &rarr; New &rarr; Import Project", navigate to directory openhitch-android
  (Android Studio should start a Gradle sync. If not so, click "File &rarr; Synchronize Project" with gradle files)
* Click "Build &rarr; Make Project"
* Open "Tools &rarr; AVD Manager" and configure a device with API 29
  (In theory we support api level >= 23, but 23 currently crashes. Need to migrate to androidx).
* Start device and set location to some point in Berlin, Germany (click on the three dots in the toolbar for the device).
* Make shure the Wifi
* The language of the virtual device may be set to Englisch or German.
* Duplicate the device (Click "Duplicate" in it's drop down menu in the rightmost column of the AVD Manager).
* At the top of the main window of Android Studio, there is a drop down list of devices. You can open it and click "Run on multiple devices" to install and run the app on both devices simultaneously.
* In the app, set the user name to "max", "moritz" or "hans" (as there is no signup and authentication yet, these are hardcoded into the database).
  To do this, go to the second screen of the app by pressing "Offer ride". In the top menu, you find a gear wheel symblos to enter the settings menu.
  Press "User Name" and enter the name. This is only needed the first time, the information survices a reinstall of the app.
