# openhitch

This is an ongoing effort to produce an open source realtime ridesharing system.

We are a small subgroup of DefelopersForFuture. Further help is highly appreciated.

For instructions on how to set up a development environment, see https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch/-/blob/master/Setup.md

Meet us on mattermost channel https://mattermost.developersforfuture.org/wvcc/channels/mitfahrapp 

We have a weekly online meeting at Tuesday 19:30, Berlin time zone. 

Please refer to max@knirz.de for invitations.